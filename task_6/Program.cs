﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
namespace task_6
{
    class Progect2
    {
        public static void fibonachi(int n)
        {
            int[] y = new int[41];
            int f = 0;
            y[0] = 0;
            y[1] = 1;
            for (int i = 2; i <= n; i++)
            {
                y[i] = y[i - 1] + y[i - 2];
                f = y[i];
            }
            Console.WriteLine($"число №{n} = {f}");
        }
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.Unicode; 
            Console.InputEncoding = Encoding.Unicode; 
            Stopwatch timer = new Stopwatch();

            Console.WriteLine("Введіть номер числа Фібоначі(до 40)");
            int n = int.Parse(Console.ReadLine());
            timer.Start();
            fibonachi(n);
            timer.Stop();
            Console.WriteLine($" Час: {timer.ElapsedMilliseconds} мілісекунд");
            Console.WriteLine($" Час: {timer.ElapsedTicks * 100} наносекунд");
            Console.ReadLine();
        }
    }
}
