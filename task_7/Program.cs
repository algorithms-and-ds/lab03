﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
namespace lab3alg
{
    class Program2
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;
            Console.WriteLine("Введіть розмір (до 40)");
            int n = int.Parse(Console.ReadLine());
            Random rnd = new Random();
            Stopwatch timer = new Stopwatch();
            int[] int32 = new int[41];
            string[] strings = new string[41];
            for (int i = 0; i <= n; i++)
            {
                int32[i] = rnd.Next(0, 512);
                strings[i] = Convert.ToString(int32[i], 2);
                Console.WriteLine($"{strings[i]}");
            }
            int max = -1;
            timer.Start();
            for (int i = 0; i <= 40; i++)
            {
                if (int32[i] > max)
                    max = int32[i];
            }
            string max2 = Convert.ToString(max, 2);
            timer.Stop();
            Console.WriteLine($" max = {max2}( {max} )");
            Console.WriteLine($" Час: {timer.ElapsedMilliseconds} мілісекунд");
            Console.WriteLine($" Час: {timer.ElapsedTicks * 100} наносекунд");
            Console.ReadKey();
        }
    }
}