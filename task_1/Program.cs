﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace task1
{
    class Program
    {
        public static void fn_n()
        {
            int[] y = new int[51];
            for (int n = 0; n <= 50; n++)
            {
                y[n] = n;
                Console.WriteLine($" y({n}) = {y[n]}");
            }
        }
        public static void fn_logn()
        {
            double[] y = new double[51];
            for (int n = 1; n <= 50; n++)
            {
                y[n] = Math.Log(n);
                Console.WriteLine($" y({n}) = {y[n]}");
            }
        }
        public static void fn_nlogn()
        {
            double[] y = new double[51];
            for (int n = 1; n <= 50; n++)
            {
                y[n] = n * Math.Log(n);
                Console.WriteLine($" y({n}) = {y[n]}");
            }
        }
        public static void fn_n2()
        {
            int[] y = new int[51];
            for (int n = 0; n <= 50; n++)
            {
                y[n] = n * n;
                Console.WriteLine($" y({n}) = {y[n]}");
            }
        }
        public static void fn_2n()
        {
            double[] y = new double[51];
            for (int n = 0; n <= 50; n++)
            {
                y[n] = Math.Pow(2, n);
                Console.WriteLine($" y({n}) = {y[n]}");
            }
        }
        public static void fn_nfactorial()
        {
            double[] y = new double[51];
            y[0] = 0;
            y[1] = 1;
            Console.WriteLine($" y(0) = {y[0]}");
            Console.WriteLine($" y(1) = {y[1]}");
            for (int n = 2; n <= 50; n++)
            {
                y[n] = n * y[n - 1];
                Console.WriteLine($" y({n}) = {y[n]}");
            }
        }
        static void Main(string[] args)
        {
            Console.OutputEncoding = System.Text.Encoding.UTF8;
            Console.InputEncoding = System.Text.Encoding.UTF8;
            Console.WriteLine("1. f(n) = n\n2. f(n) = log(n)\n3. f(n) = n log(n)\n4. f(n) =  n ^ 2\n5.f(n) = 2 ^ n\n6.f(n) = n!");
           
            int menu = int.Parse(Console.ReadLine());
            Stopwatch timer = new Stopwatch();
            switch (menu)
            {
                case 1:
                    {
                        timer.Start();
                        fn_n();
                        timer.Stop();
                        break;
                    }
                case 2:
                    {
                        timer.Start();
                        fn_logn();
                        timer.Stop();
                        break;
                    }
                case 3:
                    {
                        timer.Start();
                        fn_nlogn();
                        timer.Stop();
                        break;
                    }
                case 4:
                    {
                        timer.Start();
                        fn_n2(); 
                        timer.Stop();
                        break;
                    }
                case 5:
                    {
                        timer.Start();
                        fn_2n();
                        timer.Stop();
                        break;
                    }
                case 6:
                    {
                        timer.Start();
                        fn_nfactorial();
                        timer.Stop();
                        break;
                    }
            }
            Console.WriteLine($" Час: {timer.ElapsedMilliseconds} мілісекунд");
            Console.ReadKey();
        }
    }
}
